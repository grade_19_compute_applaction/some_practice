﻿using Car;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    namespace Car
{
        public class Car : Garage
        {
            //车具备名字的属性
            public string CarName { get; set; }
            //车具备颜色的属性
            public string CarColor { get; set; }
            //车具备轮子数的属性
            public int CarWheel { get; set; }

            public void Run()
            {
                //判断车能否起跑
                if (CarWheel >= 4)
                {
                    Console.WriteLine("可以起跑");
                }
                else
                {
                    Console.WriteLine("不能起跑");

                    Garage.Repair();

                    Console.WriteLine("请输入修车厂的名字");
                    GarageName = Console.ReadLine();

                    Console.WriteLine("请输入修车厂的电话");
                    GaragePhone = int.Parse(Console.ReadLine());

                    Console.WriteLine("请输入修车厂的地址");
                    GarageAdress = Console.ReadLine();
                }
            }
        }
    }
}
