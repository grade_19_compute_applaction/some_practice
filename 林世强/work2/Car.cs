﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work2
{
   public class Car:Garage
    {
        //车具备名字的属性。
        public string CarName { get; set; }
        //车具备颜色的属性。
        public string CarColor { get; set; }
        //车具备轮子数的属性。
        public int CarWheel { get; set; }
        //车具备跑的行为
       
        public void Run()
        {
            Console.WriteLine("车具备跑的行为");
            //判断车能否起跑
            if (CarWheel >= 4)
            {
                Console.WriteLine("车可以起跑");
            }
            else
            {
                Console.WriteLine("送去修车厂维修");
            }
                
           
        }

    }
}
