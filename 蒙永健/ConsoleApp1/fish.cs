﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Fish : Animal
    {
        public string FishName { get; set; }
        public string Fishcolor { get; set; }

        public override void Behavior()
        {
            Console.WriteLine("会动");
        }

        public void Fishbehavior()
        {
            Console.WriteLine("吹泡泡");
        }
    }
}
