﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class car:cars
    {
        public string carname { get; set; }
        public string carcolor { get; set; }
        public int carwheel { get; set; }
        public void Run()
        {
            Console.WriteLine("车子具备有跑的行为");
            if (carwheel >= 4)
            {
                Console.WriteLine("车可以跑");
            }
            else
            {
                Console.WriteLine("车送去维修厂维修");
            }
        }
    }
}
