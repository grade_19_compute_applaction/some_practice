﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car : RepaireFactory
    {
        public string CarName { set; get; }

        public string CarColor { set; get; }

        public int CarWheel { set; get; }

        public void Run()
        {
            Console.WriteLine("车子成功启动，能跑");
        }
          
}
   
}
        
