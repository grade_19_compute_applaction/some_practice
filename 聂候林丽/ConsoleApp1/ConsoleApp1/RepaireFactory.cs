﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class RepaireFactory
    {
        public string Name { set; get; }

        public string Address { set; get; }

        public string Phone { set; get; }

        public void RepaireCar()
        {
            Console.Write("维修原因：");
            Console.WriteLine("轮子坏了，需要更换！");
        }
        
}
}
