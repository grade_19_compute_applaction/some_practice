﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Animal
    {
        public string AnimalName { get; set; }
        public string AnimalColor { get; set; }

        public virtual void Move()
        {
            Console.WriteLine("所有动物都会走路！");
        }
    }
}
