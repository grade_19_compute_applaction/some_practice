﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptogram
{
    public abstract class Animals
    {
        public abstract void Name();

        public abstract void Color();

        public abstract void Run();
    }
}
