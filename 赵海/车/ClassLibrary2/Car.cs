﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class Car:Garage
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int wheel{ get; set; }


    public void Qualified()
        {
            Console.WriteLine("车是否跑起");
            if (wheel >= 4)
            {
                Console.WriteLine("车可以跑起");
            }
            else
            {
                Console.WriteLine("送去维修厂");
            }
        }
    }
}
