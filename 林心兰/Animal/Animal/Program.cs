﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入动物种类（狗或鱼）");
            string kind = Console.ReadLine();

            if(kind.Equals("狗"))
            {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的名字");
                dog.name = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色");
                dog.color = Console.ReadLine();
                dog.Bite();
            }

            if (kind.Equals("鱼"))
            {
                Fish fish = new Fish();
                Console.WriteLine("请输入鱼的名字");
                fish.name = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色");
                fish.color = Console.ReadLine();
                fish.Blow();
            }

        }
    }
}
