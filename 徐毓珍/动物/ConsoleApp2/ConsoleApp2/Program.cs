﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入是什么动物：");
            string a = Console.ReadLine();
            if (a == "狗")
            {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的名字：");
                dog.Name = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色：");
                dog.Color = Console.ReadLine();
                dog.Bite();
            }
            if (a == "鱼")
            {
                Fish fish = new Fish();
                Console.WriteLine("请输入鱼的名字：");
                fish.Name = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色：");
                fish.Color = Console.ReadLine();
                fish.Behavior();
            }
        }
    }
}
