﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Fish : Animal
    {
        //行为：吐泡泡
        public void Behavior()
        {
            Console.WriteLine("吐泡泡");
            base.Move();
        }
    }

}
