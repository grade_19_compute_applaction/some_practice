﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Garage
    {
        public int GarageNumber { get; set; }
        public string GarageName { get; set; }
        public string GarageSpace { get; set; }

        public void Repair()
        {
            Console.WriteLine("具备修车的功能行为");
        }
    }
}
