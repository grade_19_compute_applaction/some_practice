﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int CarWheel { get; set; }

        public void Run()
        {
            if (CarWheel == 4)
            {
                Console.WriteLine("车子起跑");
            }
            else
            {
                Console.WriteLine("送到修车厂");
            }
        }
    }
}
