﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp5
{
    public class Car : Garage
    {
        public string Carname { get; set; }
        public string Carcolor { get; set; }
        public int Carshu { get; set; }

        public static void run()
        {
            Console.WriteLine("车子可以跑");
        }
    }
}
