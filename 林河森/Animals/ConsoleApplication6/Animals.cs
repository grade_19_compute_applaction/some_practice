﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication6
{
    public class Animals
    {
        public string AnimalName { set; get; }
        public string AnimalColor { set; get; }

        public virtual void Move()
        {
            Console.WriteLine("都会移动");
        }
    }

}
