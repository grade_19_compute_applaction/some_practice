﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car
{
    public class Car:Garage
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int Wheel { get; set; }

        public void Start()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("可以跑起");
            }

            else
            {
                Garage.CarRepair();

                Console.WriteLine("请输入修车厂店名：");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修车厂电话号码：");
                GaragePhone = long.Parse(Console.ReadLine());
                Console.WriteLine("请输入修车厂地址：");
                GarageAddress = Console.ReadLine();
            }
        }
    }
}
