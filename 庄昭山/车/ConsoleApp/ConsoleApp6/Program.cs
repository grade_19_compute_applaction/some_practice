﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            //输入车的数据
            Car car = new Car();

            Console.WriteLine("输入车名：");
            car.CarName = Console.ReadLine();

            Console.WriteLine("输入车的颜色：");
            car.CarColor = Console.ReadLine();

            Console.WriteLine("输入车的论子数：");
            car.CarWheels = int.Parse(Console.ReadLine());
            //判断
            car.Judge();
        }
    }
}
