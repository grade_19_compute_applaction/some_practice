﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class Garage
    {
        //修车厂的属性
        public string  AgeName { get; set; }
        public string AgeAddress { get; set; }
        public string AgePhone { get; set; }
        
        //修车
        public static void Repair()
        {
            Console.WriteLine("返厂修车!");
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
