﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class Car:Garage
    {
        //车的属性
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int CarWheels { get; set; }

        
        public  void Judge()
        {
            //判断车是否合格
            if (CarWheels >= 4)
            {
                Console.WriteLine("车具备跑的行为!");
            }
            else
            {
                Garage.Repair();

                Console.WriteLine("输入修车厂的名字:");
                AgeName    = Console.ReadLine();

                Console.WriteLine("输入修车厂的地址:");
                AgeAddress = Console.ReadLine();

                Console.WriteLine("输入修车厂的电话:");
                AgePhone   = Console.ReadLine();
       
            }
            Console.WriteLine("返厂成功！");
        }
    }
}
