﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    public class Fish : Animal
    {
        public  void Behavior()
        {
            Console.WriteLine("鱼特有的行为：");

            Console.WriteLine();
            Console.WriteLine("吹泡泡");

            Console.WriteLine();
            Console.WriteLine("动物中共有的行为：");
            Console.WriteLine();
            base.Mobile();
        }
    }
}
