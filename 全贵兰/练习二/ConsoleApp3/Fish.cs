﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class Fish : Animals
    {
        public string Fname { get; set; }

        public string Fcolor { get; set; }

        public override void move()
        {
            Console.WriteLine("鱼特有行为：吹泡泡");

        }
    }
}
