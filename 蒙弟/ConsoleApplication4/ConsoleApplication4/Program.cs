﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入你要查的动物");
            string Name = Console.ReadLine();

            if (Name.Equals("狗"))
            {
                Dog dog = new Dog();
                Console.WriteLine("输入狗的名字");
                dog.AnimalName = Console.ReadLine();

                Console.WriteLine("输入狗的颜色");
                dog.AnimalColor = Console.ReadLine();

                dog.Bite();
            }
        
            if (Name.Equals("鱼"))
            {
                Fish fish = new Fish();
                Console.WriteLine("输入鱼的名字");
                fish.AnimalName = Console.ReadLine();

                Console.WriteLine("输入鱼的颜色");
                fish.AnimalColor = Console.ReadLine();

                fish.Bubbles();
            }
            else
            { Console.WriteLine("你输入的错误，请重新输入"); }
        }      
    }   
}
