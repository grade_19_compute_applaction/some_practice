﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    public class Animal
    {
        public string AnimalName { set; get; }
        public string AnimalColor { set; get; }

        public virtual void Move()
        {
            Console.WriteLine("会移动");
        }
    }
}
