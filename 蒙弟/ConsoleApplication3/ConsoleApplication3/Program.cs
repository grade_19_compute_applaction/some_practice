﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();

            Console.WriteLine("请输入车的名字：");
            car.Carname = Console.ReadLine();

            Console.WriteLine("请输入车的颜色：");
            car.Carcolor = Console.ReadLine();

            Console.WriteLine("请输入车的轮子数：");
            car.Carwheeels = int.Parse(Console.ReadLine());

            if (car.Carwheeels >= 4)
            {
                Car.run();
            }
            else
            {

                Garage.repair();
                Console.WriteLine();

                Garage add = new Garage();

                Console.WriteLine("修车厂的名字：");
                add.Garagename = Console.ReadLine();

                Console.WriteLine("请输入修车厂的地址：");
                add.Garagephone = Console.ReadLine();

                Console.WriteLine("请输入修车厂的电话：");
                add.Garagesite = Console.ReadLine();

            }
        }
    }
}
