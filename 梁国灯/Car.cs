﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    //车
    class Car : RepaireFactory
    {
        //名字
        public string CarName { set; get; }

        //颜色
        public string CarColor { set; get; }

        //轮子
        public int Wheel { set; get; }

        //车能否跑起来
        public void Run()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("成功启动");

            }
            else
            {
                Console.WriteLine("缺少轮子，需送去修车厂维修！");
                base.RepaireCar();
            }
        }
    }
}
