﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    //维修厂
    class RepaireFactory
    {
        //名字
        public string Name { set; get; }
        
        //地址
        public string Address { set; get; }

        //电话
        public string Phone { set; get; }

        //维修厂能修车
        public void RepaireCar()
        {
            Console.Write("维修原因：");
            Console.WriteLine("轮子不够，需增加轮子！");
        }
    }
}
