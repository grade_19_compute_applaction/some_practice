﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    class Fish : Animals
    {
        //名字、颜色
        public Fish(string fishName , string fishColor)
        {
            base.AnimalName = fishName;
            base.AnimalColor = fishColor;
        }

        //鱼儿是在水里游的
        public override void Move()
        {
            Console.WriteLine("鱼儿是在水里游的");
        }

        //鱼儿会吐泡泡
        public void SpitBubbles()
        {
            Console.WriteLine("鱼儿会吐泡泡！");
        }
    }
}
