﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    class Program
    {
        static void Main(string[] args)
        {


            //狗
            Dog dog = new Dog("旺柴" , "黑白相间"); //名字、颜色
            Console.WriteLine("狗狗叫" + dog.AnimalName + ",颜色是" + dog.AnimalColor);
            dog.Move();
            dog.Bite();
            Console.WriteLine();

            //鱼儿
            Fish fish = new Fish("泡泡" , "红绿色"); //名字、颜色
            Console.WriteLine("鱼儿叫" + fish.AnimalName + "，颜色是" + fish.AnimalColor);
            fish.Move();
            fish.SpitBubbles();
            Console.WriteLine();

            //车
            Car car = new Car();
            car.CarName = "法拉利";
            car.CarColor = "红色";
            car.Wheel = 1;
            car.Run();
        }
    }
}
