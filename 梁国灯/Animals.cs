﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    //动物类
    abstract class Animals
    {
        //名字
        public string AnimalName { set; get; }

        //颜色
        public string AnimalColor { set; get; }

        //移动
        public virtual void Move()
        {
            Console.WriteLine("动物都会走路！");
        }
    }
}
