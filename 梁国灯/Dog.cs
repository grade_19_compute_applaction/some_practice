﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parent_Child
{
    class Dog : Animals
    {
        //名字、颜色
        public Dog(string DogName , string DogColor)
        {
            base.AnimalName = DogName;
            base.AnimalColor = DogColor;
        }

        //狗是爬着走路
        public override void Move()
        {
            Console.WriteLine("狗是爬着走路的");
        }

        //狗会咬人
        public void Bite()
        {
            Console.WriteLine("狗会咬人！");
        }
    }
}
