﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class People : Animal
    {
        public override void Eat()
        {
            Console.WriteLine("没有什么不能吃的");
        }

        public override void Sleep()
        {
            Console.WriteLine("人类是在床上睡觉的");
        }
    }
}
