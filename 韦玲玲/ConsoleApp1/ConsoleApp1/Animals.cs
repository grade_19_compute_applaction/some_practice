﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public abstract class Animals
    {

        public string AnimalsName { get; set; }
        public string AnimalsColor { get; set; }

        public virtual void Mobile()
        {
            Console.WriteLine("移动");
        }

    }
}
