﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    public abstract class Animal
    {
        //动物具有名字的属性
        public string AnimalName { get; set; }
        //动物具有颜色的属性
        public string AnimalColor { get; set; }
        //动物具有移动的属性
        public virtual void Move()
        {
            Console.WriteLine("动物都具备的行为：");

            Console.WriteLine("移动");
        }
    }
}
