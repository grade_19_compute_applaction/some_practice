﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Garage
    {
        public string GarageName { get; set; }
        public long GaragePhone { get; set; }
        public string GarageAddress { get; set; }

        public static void CarRepair()
        {
            Console.WriteLine("送去修车厂修车！");
        }
    }
}
