﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入动物名称（狗或鱼）：");
            string dw = Console.ReadLine();

            if (dw.Equals("狗")) {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的名字：");
                dog.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色：");
                dog.AnimalColor= Console.ReadLine();
                dog.Bite();
            }

            if (dw.Equals("鱼")) {
                Fish fish = new Fish();
                Console.WriteLine("请输入鱼的名字：");
                fish.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色：");
                fish.AnimalColor = Console.ReadLine();
                fish.Blow();
            }

            Console.WriteLine("结束");
        }
    }
}
